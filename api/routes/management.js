module.exports = function (app, config) {


	/**
	 * @api {get} /management Request all users informations
	 * @apiSuccess {Array} Return user information in a array
	 */
	 app.get('/api/management', function(req, res){

	 	Users.find(function(err, data) {
	 		if(err) throw err;
	 		
	 		res.send(data);
	 	});


	 })

	}