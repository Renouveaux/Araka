module.exports = function (app, config) {


	/**
	 * @api {get} /plugins Request all plugins
	 * @apiSuccess {Array} Return plugin list
	 */
	 app.get('/api/plugins', function(req, res){

	 	Commands.find(function(err, data) {
	 		if(err) throw err;
	 		
	 		res.send(data);
	 	});


	 })

	}