var cp = require('child_process');
var bot = cp.fork(`${__dirname}/bot/index.js`);

var config = require('./config-file.json')
, express = require('express')
, cors = require('cors')
, passport = require('passport')
, session = require('express-session')
, DiscordStrategy = require('passport-discord').Strategy
, io = require('socket.io')
, app = express()
, ejwt = require('express-jwt')
, mongoose = require('mongoose')
, winston = require('winston')
, jwt = require('jsonwebtoken');

passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

require('./models');

var logger = new (winston.Logger)({
  transports: [
  new (winston.transports.Console)(),
  new (winston.transports.File)({ filename: '../application.log' })
  ]
});

mongoose.connect(config.database.host + ':' + config.database.port + '/' + config.database.base, {
  db: { native_parser: true },
  user: config.database.user || null,
  pass: config.database.pass || null
});

var scopes = ['identify', 'email', 'guilds'];

passport.use(new DiscordStrategy({
  clientID: config.discord.clientID,
  clientSecret: config.discord.clientSecret,
  callbackURL: config.discord.callbackURL,
  scope : scopes
},function(accessToken, refreshToken, profile, cb) {
  Users.findOne({ 'userId' : profile.id }, function(err, res) {
    if(res && res.role === 'admin'){
      var token = jwt.sign(res._doc, config.secret, {expiresIn : 18000});
    }
    return cb(err, token);
  });
}));

app.use(cors());

app.get('/api',
  ejwt({secret: config.secret, credentialsRequired: true}),
  function(req, res) {
    res.sendStatus(200);
  });

app.use(passport.initialize());
app.use(express.static(__dirname + '/web'));

app.get('/auth/discord', passport.authenticate('discord', { session: false, failureRedirect: '/#/login' }), function(req, res){
  res.redirect('/')
});
app.get('/auth/discord/return', passport.authenticate('discord', { session: false,
  failureRedirect: '/#/login'
}), function(req, res) {
  res.redirect("/#/profile?access_token=" + req.user);
});

require('./api/routes')(app, config);

var server = require('http').createServer(app)
, server = require('http').createServer(app)
, io = io.listen(server);

server.listen(config.port);