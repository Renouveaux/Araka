'use strict';

module.exports = function($scope, $resource, cfg){

	var plugins = $resource(cfg.apiUrl + '/plugins');

	$scope.plugins = plugins.query();

	$scope.selectPlugin = function(pluginId){
		$scope.pluginInfo = true;

		var index;
		angular.forEach($scope.plugins, function(d){
			if(d._id === pluginId){
				$scope.pluginInfos = d;
			}
		})

	}

}