'use strict';

module.exports = function($scope, $resource, cfg){

	var users = $resource(cfg.apiUrl + '/management');

	users.query(function(data){
		$scope.users = data;
	})
	
}